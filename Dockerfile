

FROM ubuntu:latest

LABEL maintainer = "Dockerfile"

RUN apt-get update && apt-get upgrade -y

RUN apt-get install python3 python3-pip git -y

RUN pip3 install --upgrade pip

RUN pip3 install flask

EXPOSE 80

CMD [“python3”, “wsgi.py”]
